from django.urls import path
from todo.views import index, del_task

urlpatterns = [
    path('', index, name= 'index'),
    path('del/<str:id>/', del_task, name='del'),
]
